<?php

namespace IDEA\Bundle\OpenIDConnectBundle\OpenIDConnect;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Interface OpenIDConnectUser
 *
 * Provide a User-modele for the OpenIDConnect bundle
 * This is used to force an User entity with a JWT, EMail, First Name, Last Name and Refresh Token
 *
 * @author Maël Mainchain
 */
interface OpenIDConnectUser extends UserInterface {
	
	/**
	 * Returns the json web token used to authenticate the OpenIDConnectUser.
	 *
	 * @return string The Json Web Token
	 */
	public function getJWT();
	
	/**
	 * Returns the email of the OpenIDConnectUser
	 *
	 * @return string The Json Web Token
	 */
	public function getEmail();
	
	/**
	 * Returns the first name of the OpenIDConnectUser
	 *
	 * @return string The Json Web Token
	 */
	public function getFirstName();
	
	/**
	 * Returns the last namme of the OpenIDConnectUser
	 *
	 * @return string The Json Web Token
	 */
	public function getLastName();
	
	/**
	 * Returns the refresh token used to reauthenticate the OpenIDConnectUser.
	 *
	 * @return string The Json Web Token
	 */
	public function getRefreshToken();
	
}