<?php

namespace IDEA\Bundle\OpenIDConnectBundle\OpenIDConnect;

use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use HWI\Bundle\OAuthBundle\OAuth\ResourceOwner\GenericOAuth2ResourceOwner;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use IDEA\Bundle\OpenIDConnectBundle\OpenIDConnect\OpenIDConnectUserResponse;
use IDEA\Bundle\OpenIDConnectBundle\OpenIDConnect\OpenIDConnectToken;

class OpenIDConnectResourceOwner extends GenericOAuth2ResourceOwner
{
	/**
	 * {@inheritdoc}
	 */
	protected $paths = array(
			'identifier' => 'user_id',
			'nickname' => 'nickname',
			'realname' => 'name',
			'email' => 'email',
			'profilepicture' => 'picture',
	);
	
	/**
	 * {@inheritdoc}
	 */
	public function getAuthorizationUrl($redirectUri, array $extraParameters = array())
	{
		return parent::getAuthorizationUrl($redirectUri, array_merge(array(
				'audience' => $this->options['audience'],
		), $extraParameters));
	}
	
	/**
	 * {@inheritdoc}
	 */
	protected function configureOptions(OptionsResolver $resolver)
	{
		parent::configureOptions($resolver);
		
		$resolver->setDefaults(array(
				'authorization_url' => '{base_url}/auth',
				'access_token_url' => '{base_url}/token',
				'infos_url' => '{base_url}/userinfo',
				'audience' => '{base_url}/userinfo',
		));
		
		$resolver->setRequired(array(
				'base_url',
		));
		
		$normalizer = function (Options $options, $value) {
			return str_replace('{base_url}', $options['base_url'], $value);
		};
		
		$resolver->setNormalizer('authorization_url', $normalizer);
		$resolver->setNormalizer('access_token_url', $normalizer);
		$resolver->setNormalizer('infos_url', $normalizer);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function getUserInformation(array $response, array $extraParameters = array())
	{
		$jwt = $this->deserializeToken($response['id_token']);
		
		// Fill response object
		$user_response = $this->getUserResponse();
		
		if(!$user_response instanceof OpenIDConnectUserResponse) {
			// TODO: throw exception
			die("Invalid UserResponse class for OpenID");
		}
		
		$user_response->setData($jwt->getClaims());
		$user_response->setResourceOwner($this);
		$user_response->setOAuthToken(new OpenIDConnectToken($response));
		
		return $user_response;
	}
	
	
	public function deserializeToken($id_token) {
		
		$public_key = file_get_contents(__DIR__ . '/../../../../src/Resources/ssl/public_key.pem');
		
		$token = (new Parser())->parse((string) $id_token);
		
		if(!$token->verify(new Sha256(), $public_key)) {
			// TODO: throw exception
			die("Invalid sign for token");
		}
		
		// TODO: verify that all claims are in the token
		
		return $token;
	}
}