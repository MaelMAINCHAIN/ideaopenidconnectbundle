<?php

namespace IDEA\Bundle\OpenIDConnectBundle\OpenIDConnect;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\OAuth\ResourceOwnerInterface;
use HWI\Bundle\OAuthBundle\Security\Core\Authentication\Token\OAuthToken;

class OpenIDConnectUserResponse implements UserResponseInterface {
	
	private $username;
	private $nickname;
	private $firstName;
	private $lastName;
	private $realName;
	private $email;
	private $profilePicture;
	private $accessToken;
	private $refreshToken;
	private $tokenSecret;
	private $expiresIn;
	private $OAuthToken;
	private $data;
	private $resourceOwner;
	
	
	/**
	 * {@inheritDoc}
	 */
	public function getUsername() {
		return $this->username;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getNickname() {
		return $this->nickname;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getFirstName() {
		return $this->firstName;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getLastName() {
		return $this->lastName;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getRealName() {
		return $this->realName;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getEmail() {
		return $this->email;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getProfilePicture() {
		return $this->profilePicture;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getAccessToken() {
		return $this->accessToken;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getRefreshToken() {
		return $this->refreshToken;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getTokenSecret() {
		return $this->tokenSecret;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getExpiresIn() {
		return $this->expiresIn;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function setOAuthToken(OAuthToken $token) {
		$this->OAuthToken = $token;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getOAuthToken() {
		return $this->OAuthToken;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getData() {
		return $this->data;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function setData($data) {
		
		if(isset($data['preferred_username'])) {
			$this->username = $data['preferred_username']->getValue();
		}
		if(isset($data['given_name'])) {
			$this->firstName = $data['given_name']->getValue();
		}
		if(isset($data['family_name'])) {
			$this->lastName = $data['family_name']->getValue();
		}
		if(isset($data['email'])) {
			$this->email = $data['email']->getValue();
		}
		
		$this->data = $data;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getResourceOwner() {
		return $this->resourceOwner;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function setResourceOwner(ResourceOwnerInterface $resourceOwner) {
		$this->resourceOwner = $resourceOwner;
		
		return $this;
	}
}