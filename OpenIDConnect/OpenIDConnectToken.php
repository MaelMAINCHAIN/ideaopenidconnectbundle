<?php

namespace IDEA\Bundle\OpenIDConnectBundle\OpenIDConnect;

use HWI\Bundle\OAuthBundle\Security\Core\Authentication\Token\OAuthToken;

// TODO: comment class
class OpenIDConnectToken extends OAuthToken {
	
	private $id_token;
	
	/**
	 * @param array|string $token The OAuth token
	 *
	 * @throws \InvalidArgumentException
	 */
	public function setRawToken($token)
	{
		parent::setRawToken($token);
		
		if (is_array($token)) {
			if (isset($token['id_token'])) {
				$this->accessToken = $token['id_token'];
			}
		}
	}
	
	/**
	 * @param string $id_token The OpenIDConenct id token
	 */
	public function setIdToken($id_token)
	{
		$this->id_token = $id_token;
	}
	
	/**
	 * @return string
	 */
	public function getIdToken()
	{
		return $this->id_token;
	}
}